<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penilaian extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct(){
        parent::__construct();
        $cek = $this->session->has_userdata('auth');
        if(!$cek) header('location:'.base_url().'admin/login');
    }
    
	public function index()
	{
        $data = [
            'title'=>'Penilaian',
            'kode_ljk' => 001,
            'no_peserta' => '001-001',
            'peserta' => 'Bambang',
            'mata_uji' => 'Algoritma Pemrograman',
            'list_soal' => [
                [
                    'no_soal' => 1,
                    'status' => false
                ],
                [
                    'no_soal' => 2,
                    'status' => false
                ],
                [
                    'no_soal' => 3,
                    'status' => false
                ],
                [
                    'no_soal' => 4,
                    'status' => false
                ],
                [
                    'no_soal' => 5,
                    'status' => false
                ],
            ],
            'current_soal'=>[
                'no_soal' => 1,
                'soal' => 'Data is passed from the controller to the view by way of an array or an object in the second parameter of the view loading method. Here is an example using an array:' ,
                'jawaban' => 'Data is passed from the controller to the view by way of an array or an object in the second parameter of the view loading method. Here is an example using an array:',
                'kriteria' => [
                    [
                        'nama' => 'Pemahaman menjawab',
                        'skor' => null,
                    ],
                    [
                        'nama' => 'Korelasi dengan soal',
                        'skor' => null,
                    ],
                    [
                        'nama' => 'Kelengkapan jawaban',
                        'skor' => null,
                    ]
                ]
            ]
        ];
        $id_ljk = $this->input->get('id_ljk');
        $no_soal = $this->input->get('no_soal');
        if(!($id_ljk && $no_soal)){
            $this->load->view('header');
            $this->load->view('penilaian',['error' => 'ID Ljk / No soal Tidak Valid']);
            $this->load->view('footer');
            return;
        }
        $this->db->select('*,dat_detail_lju.id as id_soal');
        $this->db->from('dat_detail_lju');
        $this->db->join('dat_ljk', 'dat_ljk.id = dat_detail_lju.id_ljk');
        $this->db->join('peserta', 'dat_ljk.id_peserta = peserta.id');
        $this->db->where('id_ljk',$id_ljk);
        $ljk = $this->db->get();
        if(!$ljk->result()){
            $this->load->view('header');
            $this->load->view('penilaian',['error' => 'Peserta Belum Mengisi LJK']);
            $this->load->view('footer');
            return;
        }
        $data['list_soal'] = [];
        $sample = (array)$ljk->result()[0];
        $data['kode_ljk'] = $sample['kode'];
        $data['peserta'] = $sample['nama_peserta'];
        $data['no_peserta'] = $sample['no_peserta'];
        $data['mata_uji'] = $sample['mata_uji'];
        $sample_kriteria = [
            [
                'nama' => 'pemahaman_menjawab',
                'skor' => null,
            ],
            [
                'nama' => 'korelasi_dengan_soal',
                'skor' => null,
            ],
            [
                'nama' => 'kelengkapan_jawaban',
                'skor' => null,
            ]
            ];
        foreach($ljk->result() as $row){
            $row = (array)$row;
            $data['list_soal'][] = [
                'id' => $row['id_soal'],
                'id_ljk' => $id_ljk,
                'no_soal' => $row['no_soal'],
                'status' => (boolean) $row['status'],
            ];
            if(((int)$row['no_soal']) === ((int)$no_soal)){
                $data['current_soal'] = [
                    'no_soal' => $row['no_soal'],
                    'id_detail_ljk' => $row['id_soal'],
                    'soal' =>  $row['soal'],
                    'jawaban' => $row['jawaban'],
                    'kriteria' => json_decode($row['kriteria'])?json_decode($row['kriteria']):$sample_kriteria
                ];
            }
        }
		$this->load->view('header',['current' => 'data_ljk']);
		$this->load->view('penilaian',$data);
		$this->load->view('footer');
	}

    public function save(){
        $kiteria = [
            [
                'nama' => 'pemahaman_menjawab',
                'skor' => $this->input->post('pemahaman_menjawab'),
            ],
            [
                'nama' => 'korelasi_dengan_soal',
                'skor' => $this->input->post('korelasi_dengan_soal'),
            ],
            [
                'nama' => 'kelengkapan_jawaban',
                'skor' => $this->input->post('kelengkapan_jawaban'),
            ]
        ];
        $data = array(
                'kriteria' => json_encode($kiteria),
                'soal' => $this->input->post('soal'),
                'jawaban' => $this->input->post('jawaban')
        );
        $this->db->where('id', $this->input->post('id_detail_ljk'));
        $this->db->update('dat_detail_lju', $data);

        header('location:'.$_SERVER['HTTP_REFERER']);
    }
}
