<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct(){
        parent::__construct();
        $cek = $this->session->has_userdata('auth');
        if(!$cek) header('location:'.base_url().'admin/login');
    }
	public function index(){
        $this->db->select('*');
        $this->db->from('peserta');
        $ljk = $this->db->get();
        $data = [
            'title'=>'Data Peserta',
            'tableData' => $ljk->result()
        ];

        $this->load->view('header',['current' => 'data_peserta']);
		$this->load->view('data_peserta',$data);
		$this->load->view('footer');
    }

    public function save(){
        echo 'hello';
    }
}
