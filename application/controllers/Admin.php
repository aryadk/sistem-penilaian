<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
        $cek = $this->session->has_userdata('auth');
        if(!$cek) header('location:'.base_url().'admin/login');
        $this->db->select('*');
        $this->db->from('dat_admin');
        $admin = $this->db->get();
        $data = [
            'title'=>'Data Admin',
            'tableData' => $admin->result()
        ];

        $this->load->view('header',['current' => 'data_admin']);
		$this->load->view('data_admin',$data);
		$this->load->view('footer');
    }

    public function form(){
        $cek = $this->session->has_userdata('auth');
        if(!$cek) header('location:'.base_url().'admin/login');
        $data = [
            [
                'name' => 'nama',
                'label' => 'Nama',
                'type' => 'text'
            ],
            [
                'name' => 'username',
                'label' => 'Username',
                'type' => 'text'
            ],
            [
                'name' => 'password',
                'label' => 'Password',
                'type' => 'password'
            ]
        ];
        $data = [
            'title' => 'Tambah Admin',
            'form' => $data
        ];
        $this->load->view('header',['current' => 'data_admin']);
		$this->load->view('form_admin',$data);
		$this->load->view('footer');
    }
    public function save(){
        $cek = $this->session->has_userdata('auth');
        if(!$cek) header('location:'.base_url().'admin/login');
        $data = array(
                'nama' => $this->input->post('nama'),
                'username' => $this->input->post('username'),
                'password' => md5($this->input->post('password')),
                'status' => 1
        );
        $this->db->insert('dat_admin', $data);

        header('location:'.$_SERVER['HTTP_REFERER']);
    }

    public function login(){
        
        $data = [
            'form' => [
                [
                    'name' => 'username',
                    'type' => 'text'
                ],
                [
                    'name' => 'password',
                    'label' => 'Password',
                    'type' => 'password'
                ]
            ],
        ];
		$this->load->view('login');
    }

    public function loginAction(){
        $data = array(
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
        );

        $this->db->select('*');
        $this->db->from('dat_admin');
        $this->db->where('username',$data['username']);
        $this->db->where('password',$data['password']);
        $admin = $this->db->get();
        if(!$admin->result()){
            $data = [
                [
                    'name' => 'username',
                    'type' => 'text'
                ],
                [
                    'name' => 'password',
                    'label' => 'Password',
                    'type' => 'password'
                ]
            ];
            $this->load->view('login',['error'=>'Username / Password Salah']);
            return;
        }
        $this->session->set_userdata('auth',json_encode($admin->result()) );
        header('location:'.base_url().'ljk');
    }

    public function logout(){
        $this->session->unset_userdata('auth');
        header('location:'.base_url().'admin/login');
    }
}
