<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ljk extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct(){
        parent::__construct();
        $cek = $this->session->has_userdata('auth');
        if(!$cek) header('location:'.base_url().'admin/login');
    }

	public function index(){
        $this->db->select('*,dat_ljk.id as id_ljk');
        $this->db->from('dat_ljk');
        $this->db->join('peserta', 'dat_ljk.id_peserta = peserta.id');
        $ljk = $this->db->get();
        $data = [
            'title'=>'Data Ljk',
            'tableData' => $ljk->result()
        ];

        $this->load->view('header',['current' => 'data_ljk']);
		$this->load->view('data_ljk',$data);
		$this->load->view('footer');
    }

    public function save(){
        echo 'hello';
    }
}
