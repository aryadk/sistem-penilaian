  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><?= $title ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><?= $title ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content mb-2">
        <div class="container">
            <table class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode LJK</th>
                        <th>Mata Uji</th>
                        <th>Peserta</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($tableData as $key => $row):?>
                    <tr>
                        <td><?= $key+1?></td>
                        <td><?= $row->kode?></td>
                        <td><?= $row->mata_uji?></td>
                        <td><?= $row->no_peserta .' - '.$row->nama_peserta?></td>
                        <td><a href="<?php echo base_url(); ?>penilaian?no_soal=<?=1?>&id_ljk=<?=$row->id_ljk?>" class="btn-sm btn-warning"><i class="fa fa-edit"></i></a></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  