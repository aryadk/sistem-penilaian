  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><?= $title ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><?= $title ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content mb-2">
        <div class="container mx-auto">
            <form action="<?php echo base_url(); ?>admin/save" method="post">
                <?php foreach($form as $v):?>
                    <div class="form-group row">
                        <label for="" class="col-2"><?= $v['label'] ?></label>
                        <div class="col-10">
                            <input type="<?= $v['type'] ?>" name="<?= $v['name'] ?>" class="form-control">
                        </div>
                    </div>
                <?php endforeach;?>
                <div class="text-right">
                    <button class="btn btn-primary btn-sm ml-auto" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  