  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php if(isset($error)):?>
        <div class="p-3">
            <div class="alert alert-danger">
                <?= $error ?>
            </div>
        </div>
    <?php else:?>
    
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><?= $title ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><?= $title ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content mb-2">
        <div class="px-3 d-flex mb-3">
            <span class="mr-4"> <b>Mata Uji :</b>  <?= $mata_uji?> </span>
            <span class="mr-4"> <b>Kode Ljk :</b>  <?= $kode_ljk?> </span>
            <span class="mr-4"><b>Peserta :</b> <?= $no_peserta .' - '.$peserta?></span>
        </div>
        <div class="row">
            <div class="col-3 px-3">
                <div class="list-group">
                    <?php foreach( $list_soal as $v):
                    $active = $v['no_soal'] == $current_soal['no_soal']?' active':'';
                    ?>
                    <a href="?<?php echo 'no_soal='.$v['no_soal'].'&id_ljk='.$v['id_ljk']?>" class="list-group-item list-group-item-action <?php echo $active?>">
                        Nomor <?= $v['no_soal'];?>
                        <?php if($v['status']):?>
                            <i class="fa fa-check"></i>
                        <?php endif;?>
                    </a>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-9">
                <form action="<?php echo base_url(); ?>penilaian/save" method="post">
                    <div class="text-right">
                        <button class="btn btn-primary btn-sm ml-auto w-25" type="submit">Submit</button>
                    </div>
                    <div>
                        <input type="hidden" name="id_detail_ljk" value="<?php echo $current_soal['id_detail_ljk']?>">
                        <div class="form-group">
                            <label for="">Soal</label>
                            <textarea  id="" cols="30" rows="3" name="soal" class="form-control" ><?php echo $current_soal['soal']?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Jawaban :</label>
                            <textarea id="" cols="30" name="jawaban" rows="3" class="form-control" ><?php echo $current_soal['jawaban']?></textarea>
                        </div>
                    </div>
                    <hr>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Kriteria</th>
                                <th class="w-25">Skor (%)</th>
                            </tr>
                        </thead>
                        <tbody>
                                <?php foreach($current_soal['kriteria'] as $v): $v = (array)$v; ?>
                                    <tr>
                                        <td><?= ucfirst(str_replace('_',' ',$v['nama']));?></td>
                                        <td><input type="number" name="<?php echo $v['nama']?>" class="form-control-sm" value="<?= $v['skor'];?>"></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                    </table>
                    <div class="text-right">
                        <button class="btn btn-primary btn-sm ml-auto w-25" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <?php endif; ?>
  </div>
  <!-- /.content-wrapper -->

  